import typer
from typing import Annotated
from enum import Enum
import sys
from contextlib import contextmanager
import requests
import json
import subprocess
from os.path import join, dirname
from click import Context
from typer.core import TyperGroup
from dotenv import load_dotenv
import html

load_dotenv()


# https://github.com/tiangolo/typer/issues/428#issuecomment-1238866548
class OrderCommands(TyperGroup):
    def list_commands(self, ctx: Context):
        """Return list of commands in the order appear."""
        return list(self.commands)  # get commands using self.commands


app = typer.Typer()
lexicala_app = typer.Typer(
    cls=OrderCommands, help="Fetch info from the Lexicala API and load it into Anki"
)
app.add_typer(lexicala_app, name="lexicala")
deepl_app = typer.Typer(
    cls=OrderCommands, help="Fetch info from the DeepL API and load it into Anki"
)
app.add_typer(deepl_app, name="deepl")


class Languages(str, Enum):
    nl = "nl"
    en = "en"
    th = "th"


LEXICALA_URL = "https://lexicala1.p.rapidapi.com/search-entries"
DEEPL_URL = "https://api-free.deepl.com/v2/translate"


@contextmanager
def smart_open(filename=None, mode="r"):
    if filename and filename != "-":
        # Open a file normally
        f = open(filename, mode)
        try:
            yield f
        finally:
            f.close()
    else:
        # Yield standard input or output without closing it
        yield sys.stdin if mode == "r" else sys.stdout


@lexicala_app.command("pull")
def lexicala_pull(
    api_key: Annotated[
        str,
        typer.Option(help="API key for the Lexicala API", envvar="LEXICALA_API_KEY"),
    ],
    input_file: Annotated[
        str,
        typer.Option(
            "--input",
            "-i",
            help="Path to a file containing a list of words to look up using the Lexicala API, one per line",
        ),
    ] = "-",
    output_file: Annotated[
        str,
        typer.Option(
            "--output",
            "-o",
            help="Path to a file where the results from the API will be written, as a JSONL file",
        ),
    ] = "-",
    rapidapi_host: Annotated[
        str, typer.Option(envvar="RAPID_API_HOST")
    ] = "lexicala1.p.rapidapi.com",
    language: Annotated[
        Languages,
        typer.Option(
            "--source-language", "-s", help="Source language in the Lexicala API"
        ),
    ] = Languages.nl,
):
    """Fetch information about the words in the given file from the Lexicala API"""
    with smart_open(input_file, "r") as input, smart_open(output_file, "w") as output:
        for word in input.readlines():
            headers = {"X-RapidAPI-Key": api_key, "X-RapidAPI-Host": rapidapi_host}
            params = {
                "text": word.strip(),
                "language": language.value,
                "analyzed": "true",
                **({"source": "password" if language == Languages.en else {}}),
            }
            response = requests.get(LEXICALA_URL, headers=headers, params=params)
            output.write(json.dumps({word.strip(): response.json()}) + "\n")


@lexicala_app.command("transform")
def lexicala_transform(
    input_file: Annotated[
        str,
        typer.Option(
            "--input",
            "-i",
            help="Path to a JSONL file containing results of lookups in the Lexicala API",
        ),
    ] = "-",
    output_file: Annotated[
        str,
        typer.Option(
            "--output",
            "-o",
            help="Path to a file where the results from the API will be written, as a JSONL file",
        ),
    ] = "-",
    source_language: Annotated[
        Languages,
        typer.Option(
            "--source-language", "-s", help="Source language in the Lexicala API"
        ),
    ] = Languages.nl,
    target_language: Annotated[
        Languages,
        typer.Option("--target-language", "-t", help="Desired target"),
    ] = Languages.en,
    phrases: Annotated[
        bool,
        typer.Option(help="Extract phrases instead of words from the Lexicala data"),
    ] = False,
):
    """Transform the given input from the Lexicala API to turn it into invidual entries for a flashcard each using jq"""
    with smart_open(input_file, "r") as input, smart_open(output_file, "w") as output:
        jq_directory = join(join(dirname(__file__), "../jq"))
        jq_script = (
            f"lexicala/words/{source_language.value}_to_{target_language.value}.jq"
            if not phrases
            else f"lexicala/phrases/{target_language.value}.jq"
        )
        subprocess.run(
            [
                "jq",
                "-cr",
                "-L",
                jq_directory,
                "-f",
                join(jq_directory, jq_script),
            ],
            input=input.read().encode(),
            stdout=output,
            stderr=output,
        )


@lexicala_app.command("load")
def lexicala_load(
    input_file: Annotated[
        str,
        typer.Option(
            "--input", "-i", help="Path to a JSONL file with words (or phrases)"
        ),
    ] = "-",
    anki_connect_host: Annotated[
        str, typer.Option(envvar="ANKI_CONNECT_HOST")
    ] = "localhost",
    anki_connect_port: Annotated[int, typer.Option(envvar="ANKI_CONNECT_PORT")] = 8765,
    anki_note_type: Annotated[
        str, typer.Option("--anki-note-type", "-t")
    ] = "Bilingual",
    anki_deck: Annotated[
        str, typer.Option("--anki-deck", "-d")
    ] = "Languages::Dutch::Words",
    phrases: Annotated[
        bool,
        typer.Option(
            help="Parse data as phrases instead of words from the Lexicala data. Also disables duplicates"
        ),
    ] = False,
):
    """Load the results from the given file into Anki"""
    options_presets = {
        "duplicates": {
            True: {
                "allowDuplicate": True,
            },
            False: {
                "allowDuplicate": False,
                "duplicateScope": "deck",
                "duplicateScopeOptions": {
                    "deckName": anki_deck,
                    "checkChildren": False,
                    "checkAllModels": False,
                },
            },
        }
    }
    with smart_open(input_file, "r") as input:
        for json_object in input.readlines():
            result = json.loads(json_object)
            fields = (
                {
                    "Original": result["headword"].capitalize(),
                    "Examples": "<br>".join(
                        [example.capitalize() for example in result["examples"]]
                    ),
                    "Definition": result["definition"].capitalize(),
                    "English": result["translation"].capitalize(),
                    "Add Reverse": "y",
                }
                if not phrases
                else {
                    "Phrase": result["phrase"].capitalize(),
                    "Definition": html.escape(result["definition"].capitalize()),
                    "English": result["translation"].capitalize(),
                    "Add Reverse": "y",
                }
            )
            request = {
                "action": "addNote",
                "version": 6,
                "params": {
                    "note": {
                        "deckName": anki_deck,
                        "modelName": anki_note_type,
                        "fields": fields,
                        "options": options_presets["duplicates"][not phrases],
                        "tags": ["lexicala"],
                    }
                },
            }
            response = requests.post(
                f"http://{anki_connect_host}:{anki_connect_port}", json=request
            )
            print(f"result: {response.json()}")


@deepl_app.command("pull")
def deepl_pull(
    api_key: Annotated[
        str,
        typer.Option(help="API key for the DeepL API", envvar="DEEPL_API_KEY"),
    ],
    input_file: Annotated[
        str,
        typer.Option(
            "--input",
            "-i",
            help="Path to a file containing a list of expressions to look up using the DeepL API, one per line",
        ),
    ] = "-",
    output_file: Annotated[
        str,
        typer.Option(
            "--output",
            "-o",
            help="Path to a file where the results from the API will be written, as a JSONL file",
        ),
    ] = "-",
    source_language: Annotated[
        Languages,
        typer.Option(
            "--source-language", "-s", help="Source language in the DeepL API"
        ),
    ] = Languages.en,
    target_language: Annotated[
        Languages,
        typer.Option(
            "--target-language", "-t", help="Target language in the DeepL API"
        ),
    ] = Languages.nl,
):
    """Fetch information about the expressions in the given file from the DeepL API"""
    with smart_open(input_file, "r") as input, smart_open(output_file, "w") as output:
        for expression in input.readlines():
            headers = {"Authorization": f"DeepL-Auth-Key {api_key}"}
            params = {
                "text": [expression.strip()],
                "source_lang": source_language.value.upper(),
                "target_lang": target_language.value.upper(),
            }
            response = requests.post(DEEPL_URL, headers=headers, params=params)
            output.write(json.dumps({expression.strip(): response.json()}) + "\n")


@deepl_app.command("transform")
def deepl_transform(
    input_file: Annotated[
        str,
        typer.Option(
            "--input",
            "-i",
            help="Path to a JSONL file containing results of lookups in the DeepL API",
        ),
    ] = "-",
    output_file: Annotated[
        str,
        typer.Option(
            "--output",
            "-o",
            help="Path to a file where the results from the API will be written, as a JSONL file",
        ),
    ] = "-",
):
    """Transform the given input from the DeepL API to turn it into invidual entries for a flashcard each using jq"""
    with smart_open(input_file, "r") as input, smart_open(output_file, "w") as output:
        jq_directory = join(join(dirname(__file__), "../jq"))
        subprocess.run(
            ["jq", "-crf", join(jq_directory, "deepl/main.jq")],
            input=input.read().encode(),
            stdout=output,
            stderr=output,
        )


@deepl_app.command("load")
def deepl_load(
    input_file: Annotated[
        str,
        typer.Option("--input", "-i", help="Path to a JSONL file with expressions"),
    ] = "-",
    anki_connect_host: Annotated[
        str, typer.Option(envvar="ANKI_CONNECT_HOST")
    ] = "localhost",
    anki_connect_port: Annotated[int, typer.Option(envvar="ANKI_CONNECT_PORT")] = 8765,
    anki_note_type: Annotated[
        str, typer.Option()
    ] = "Basic (Hint+optional reversed card)",
    anki_deck: Annotated[str, typer.Option()] = "Languages::Dutch::Expressions",
    invert: Annotated[bool, typer.Option(help="Invert front and back")] = False,
):
    """Load the results from the given file into Anki"""
    with smart_open(input_file, "r") as input:
        for json_object in input.readlines():
            result = json.loads(json_object)
            front = list((result.keys if not invert else result.values)())[
                0
            ].capitalize()
            back = list((result.values if not invert else result.keys)())[
                0
            ].capitalize()
            request = {
                "action": "addNote",
                "version": 6,
                "params": {
                    "note": {
                        "deckName": anki_deck,
                        "modelName": anki_note_type,
                        "fields": {
                            "Front": front,
                            "Back": back,
                            "Add Reverse": "y",
                        },
                        "options": {
                            "allowDuplicate": False,
                            "duplicateScope": "deck",
                            "duplicateScopeOptions": {
                                "deckName": anki_deck,
                                "checkChildren": False,
                                "checkAllModels": False,
                            },
                        },
                        "tags": ["deepl"],
                    }
                },
            }
            response = requests.post(
                f"http://{anki_connect_host}:{anki_connect_port}", json=request
            )
            print(f"result: {response.json()}")


if __name__ == "__main__":
    app()
