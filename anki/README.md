# How to define the Bilingual note type

1. In Anki, go to Tools > Manage Note Types and click Add. You can start by cloning any note type with a reverse card, for example Basic (and reversed card). Name it Bilingual: ![Add note](./img/add_note_type.png) ![Bilingual](./img/bilingual.png)
2. In the note types menu, select Bilingual and click on Fields: ![Note types](./img/note_types_menu.png) Add the five fields in the screenshot. They all have the same default settings for things like font and such, and Original is used as the sort field: ![Fields](./img/fields.png)
3. In the note types menu, select Bilingual and click on Cards. Copy the contents of `templates/card1_front_template.html` in the text area for the template. Then, click on Back template, and do the same with `templates/card1_back_template.html`. Finally, copy the contents of `syling.css` in the text area in the Styling tab (**Note**: the CSS files defines a few more properties than necessary for the Bilingual notes to look the way they do. Those were defined to make cards layout more responsive, to ensure a good experience on mobile).
![Card 1 Front Template](./img/card1_front_template.png)
![Card 1 Back Template](./img/card1_back_template.png)
4. In the the same window, click on the Card Type dropdown and change to Card 2. Copy the contents of `templates/card2_front_template.html` in the text area for the template. Then, click on Back template, and do the same with `templates/card2_back_template.html`. Finally, copy the contents of `syling.css` in the text area in the Styling tab.
![Card 2 Front Template](./img/card2_front_template.png)
![Card 2 Back Template](./img/card2_back_template.png)
