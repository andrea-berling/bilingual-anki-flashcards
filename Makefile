.PHONY: pull-lexicala-nl transform-nl load-nl transform-nl-phrases load-nl-phrases pull-lexicala-en

SHELL := /bin/bash

pull-lexicala:
	wc -l data/lexicala/raw/$(LANGUAGE)/lexicala_collection.jsonl && python src/main.py lexicala pull -i $(LANGUAGE)_words.txt -s $(LANGUAGE) >> data/lexicala/raw/$(LANGUAGE)/lexicala_collection.jsonl

transform:
	python src/main.py lexicala transform -s $(LANGUAGE) -i <(awk 'NR >= $(LINE_NUMBER)' <data/lexicala/raw/$(LANGUAGE)/lexicala_collection.jsonl) -o transformed_output.jsonl $(EXTRA_OPTIONS)

load-nl:
	python src/main.py lexicala load -i transformed_output.jsonl $(EXTRA_OPTIONS)

pull-lexicala-nl transform-nl: LANGUAGE=nl
pull-lexicala-en transform-en: LANGUAGE=en
pull-lexicala-nl pull-lexicala-en: pull-lexicala
transform-nl transform-en: transform
transform-en: EXTRA_OPTIONS=-t nl



transform-nl-phrases: EXTRA_OPTIONS=--phrases
transform-nl-phrases: transform-nl
load-nl-phrases: EXTRA_OPTIONS=--phrases -d 'Languages::Dutch::Expressions' -t 'Bilingual Phrase'
load-nl-phrases: load-nl
