to_entries[]
| {
    "\(.key)" : ([.value.translations[] | .text] | join("/"))
  }
