def extract_from_possible_array(extractor;joiner): if (. | type) == "array" then ([ .[] | extractor] | joiner) else (. | extractor) end;

def extract_from_possible_array(extractor): extract_from_possible_array(extractor;. | join("/"));

def get_dutch_prefix: if .[0] == "noun" then if .[1] == "neuter" then "Het " else "De " end else if .[2] == "reflexive" then "Zich " else "" end end;

def get_english_prefix: if .[0] == "verb" then "To " elif .[0] == "noun" then "The " else "" end;

def no_prefix: "";

def break_lexicala_results_down(translations_extractor;src_prefix_getter;dst_prefix_getter):
  to_entries[]
  | .key as $lookup
  | .value.results[]
  | select(.senses)
  | (.headword | extract_from_possible_array(.text)) as $headword
  | (.headword | extract_from_possible_array(.pos)) as $pos
  | (.headword | extract_from_possible_array(.gender)) as $gender
  | (.headword | extract_from_possible_array(.valency)) as $valency
  | .senses[] | (.translations | translations_extractor) as $translations
  | select(.definition)
  | {
      "lookup" : $lookup,
      "headword" : "\([$pos,$gender,$valency] | dst_prefix_getter)\($headword)",
      "definition": .definition,
      "examples": (if .examples then [.examples[].text] else [] end),
      "translation" : "\([$pos,$gender] | src_prefix_getter)\($translations | extract_from_possible_array(.text))"
    };

def break_lexicala_phrases_down(translations_extractor):
  to_entries[]
  | .key as $lookup
  | .value.results[]
  | select(.senses)
  | .senses[]
  | select(.compositional_phrases)
  | .compositional_phrases[]
  | (.translations | translations_extractor) as $translations
  | select(.definition)
  | {
      "lookup" : $lookup,
      "phrase": .text,
      "definition": .definition,
      "translation" : "\($translations | extract_from_possible_array(.text))"
    };
