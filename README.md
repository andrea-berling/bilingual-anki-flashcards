# Anki bilingual cards generator

Repository with the code for a python application to automatically generate Anki flashcards for language learning using the Lexicala and DeepL API. The whole system is described in great detail in this [article](https://medium.com/@andrea.berlingieri42/automatically-generating-anki-flashcards-for-language-learning-using-the-lexicala-api-and-deepl-412846986186).

## Getting started

You will need the following to run this project:
- `python3`
- `jq`

If you have [Nix](https://nixos.org/) installed, you can quickly get into a shell with all the needed setup by running `nix-shell`.

For anybody else, create a new python virtual environment (`python3 -m venv venv`), activate it (`source venv/bin/activate`), and install the requirements (`pip install -r requirements.txt`).

The application is powered by [Typer](https://typer.tiangolo.com/). In particular, it's easy to dive into it and understand what can be done by running `python src/main.py --help`:
![Python main help](./img/python_main_help.png)

Every subcommand also supports `--help` and each option is documented, along with their defaults and environment variables that can be used to set them.

# Example flow to generate flashcards from a list of dutch words

1. Set your environment up to query the Lexicala API:
   ```
   export LEXICALA_API_KEY=<your-api-key>
   ```

2. Pull data about the list of words you compiled from Lexicala:
   ```
   python src/main.py lexicala pull -i list_of_dutch_words.txt -o lexicala_data.jsonl
   ```

   Assuming your list of words contains the word "eeuwig", you should get something similar to `examples/lexicala/eeuwig.jsonl`

3. Transform the data to make it easier to review for flashcards creation:
   ```
   python src/main.py lexicala transform -i lexicala_data.jsonl -o lexicala_transformed_data.jsonl
   ```

4. Review the transformed data and prune entries that you are not interested into converting into flashcards

5. Finally, make sure Anki is running and that the AnkiConnect add-on is installed, and load the transformed and reviewed data into Anki (changing settings like the note type and the deck accordingly):
   ```
   python src/main.py lexicala load -i lexicala_data.jsonl
   ```

A similar flow can be used to create flashcards from english words (possibly by feeding the dutch translations of the english words back into the flow above) and for expressions (this time using the deepl subcommands).



